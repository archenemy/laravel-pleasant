<?php namespace Archenemy\Pleasant;

class Like extends \Eloquent {

    protected $table = 'pleasant_likes';
    public $timestamps = true;
    protected $fillable = ['likeable_id', 'likeable_type', 'user_id', 'value'];

    public function likeable() {
        return $this->morphTo();
    }

}
