<?php namespace Archenemy\Pleasant;

interface PleasantInterface {

    /**
     * Returns the URL for liking the current element.
     * @return string Like URL for the current element.
     */
    function getLikeUrl();
    /**
     * Returns the URL for disliking the current element.
     * @return string Dislike URL for the current element.
     */
    function getDislikeUrl();
    
}
